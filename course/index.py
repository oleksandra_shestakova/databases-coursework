import psycopg2
import matplotlib.pyplot as plt
connect = psycopg2.connect(user='postgres',password='DipperS13',host='localhost',port='5432',database='USWeather')
cursor=connect.cursor()
results_without=[]
cursor.execute('drop index type_index ')
connect.commit()
for j in range(30):
    cursor.execute("explain analyze select * from weather_events where type=4 ")
    connect.commit()
    res = cursor.fetchall()
    for i in res:
        if(i[0].find('Execution')>-1):
            result=None
            for j in i[0].split():
                try:
                    result = float(j)
                    break
                except:
                    continue
            results_without.append(result)
connect.commit()
cursor.execute('create index type_index on weather_events(type)')

results_with=[]
for j in range(30):
    cursor.execute("explain analyze select * from weather_events where type=4")
    connect.commit()
    res = cursor.fetchall()
    for i in res:
        if(i[0].find('Execution')>-1):
            result=None
            for j in i[0].split():
                try:
                    result = float(j)
                    break
                except:
                    continue
            results_with.append(result)
plt.plot(results_without)
plt.plot(results_with)
plt.show()
cursor.close()
connect.close()
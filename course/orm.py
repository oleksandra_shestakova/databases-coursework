import psycopg2
from datetime import time, timedelta, datetime
import pandas as pd
import matplotlib.pyplot as pplt
from entities import *
from sqlalchemy import create_engine, select, exists, exc
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from datetime import datetime

Base = declarative_base()


class DataBase:

    def connect(self):
        self.engine = create_engine('postgresql://postgres:DipperS13@localhost:5432/USWeather')
        self.Session = sessionmaker(bind=self.engine)
        self.session = self.Session()

    def close(self):
        self.session.close()
        print("Connection to the database is closed.")

    def GetEntityById(self, entityName, id):
        entity = -1
        if entityName == "Cities":
            entity = self.session.query(Cities).get(id)
        if entityName == "Counties":
            entity = self.session.query(Counties).get(id)
        if entityName == "States":
            entity = self.session.query(States).get(id)
        if entityName == "Severities":
            entity = self.session.query(Severities).get(id)
        if entityName == "Types":
            entity = self.session.query(Types).get(id)
        if entityName == "WeatherEvents":
            entity = self.session.query(WeatherEvents).get(id)
        i = 0
        for attr, value in entity.__dict__.items():
            if i == 0:
                i += 1
                continue
            print(attr, ": ", value)
        return entity

    # -------add-------
    def AddEntity(self, entity):
        try:
            self.session.add(entity)
            self.session.commit()
            print("Entity added successfully.")
        except exc.SQLAlchemyError:
            print("Problem adding entity.")
            return False
        return entity.id

    # -------update-------
    def UpdateCity(self, city):
        try:
            some_city = self.GetEntityById("Cities", city.id)
            some_city.city_name = some_city.city_name
            some_city.county = some_city.county
            some_city.zip_code = some_city.zip_code
            some_city.time_zone = some_city.time_zone
            self.session.commit()
        except exc.SQLAlchemyError:
            print("Problem updating city.")
            return False
        return True

    def UpdateCounty(self, county):
        try:
            some_county = self.GetEntityById("Counties", county.id)
            some_county.county_name = county.county_name
            some_county.state = county.state
            self.session.commit()
        except exc.SQLAlchemyError:
            print("Problem updating county.")
            return False
        return True

    def UpdateState(self, state):
        try:
            some_state = self.GetEntityById("States", state.id)
            some_state.state_name = state.state_name
            self.session.commit()
        except exc.SQLAlchemyError:
            print("Problem updating state.")
            return False
        return True

    def UpdateSeverity(self, severity):
        try:
            some_severity = self.GetEntityById("Severities", severity.id)
            some_severity.severity_name = severity.severity_name
            self.session.commit()
        except exc.SQLAlchemyError:
            print("Problem updating severity.")
            return False
        return True

    def UpdateType(self, type):
        try:
            some_type = self.GetEntityById("Types", type.id)
            some_type.type_name = type.type_name
            self.session.commit()
        except exc.SQLAlchemyError:
            print("Problem updating type.")
            return False
        return True

    def UpdateWeatherEvent(self, weatherevent):
        try:
            some_weatherevent = self.GetEntityById("WeatherEvents", weatherevent.id)
            some_weatherevent.start_time = weatherevent.start_time
            some_weatherevent.end_time = weatherevent.end_time
            some_weatherevent.airport_code = weatherevent.airport_code
            some_weatherevent.location_lat = weatherevent.location_lat
            some_weatherevent.location_lng = weatherevent.location_lng
            some_weatherevent.severity = weatherevent.severity
            some_weatherevent.type = weatherevent.type
            some_weatherevent.city = weatherevent.city
            self.session.commit()
        except exc.SQLAlchemyError:
            print("Problem updating weather event.")
            return False
        return True

    # -------delete-------
    def DeleteEntity(self, entity):
        check = 0

        if entity.__class__.__name__ == "Counties":
            check = self.session.query(Cities.id).filter_by(county=Cities.county).count()
        elif entity.__class__.__name__ == "States":
            check = self.session.query(Counties.id).filter_by(state=Counties.state).count()
        elif entity.__class__.__name__ == "WeatherEvents":
            check = self.session.query(WeatherEvents.id).filter_by(city=WeatherEvents.city, type=WeatherEvents.type,
                                                                   severity=WeatherEvents.severity).count()

        if check!=0:
            print(f"Entity with given id was not deleted. Please, firstly delete dependent entity: {check}.")
            return False
        try:
            self.session.delete(entity)
            self.session.commit()
            print("Deleted entity.")
        except exc.SQLAlchemyError:
            print("Problem deleting entity.")
            return False
        return True

    # -------generate-------
    def GenerateWeatherEvent(self, amount):
        print(f"starting to generate {amount} of records in weather events table.")

        self.session.execute(f"insert into weather_events (start_time, end_time, airport_code, location_lat, "
                             f" location_lng, severity, type, city)"
                             f" select rnd.start_time, rnd.end_time, rnd.airport_code, rnd.location_lat,"
                             f" rnd.location_lng, rnd.severity, rnd.type, rnd.city"
                             f" from (select now()::timestamp - '1 hour'::interval * round(random() * 5) as start_time,"
                             f" now()::timestamp + '1 hour'::interval * round(random() * 15) "
                             f" + '1 second'::interval * round(random() * 60) as end_time,"
                             f" SUBSTR('ABCDEFGHIJKLMNOPQRSTUVWXYZ',randomNumber(1,26)::INTEGER, 5) as airport_code,"
                             f" (-90 + (90 + 90) * RANDOM())::NUMERIC as location_lat,"
                             f" (-180 + (180 + 180) * RANDOM())::NUMERIC as location_lng, "
                             f" severities.id as severity,"
                             f" types.id as type,"
                             f" cities.id as city from severities, types, cities)"
                             f" as rnd left join weather_events on(rnd.severity=weather_events.severity and rnd.type=weather_events.type "
                             f" and rnd.city=weather_events.city) where weather_events.id is NULL order by random() limit {amount}")
        self.session.commit()
        print(f"Generated {amount} of records in weather events table.")
        return True

    # -------filter-------
    def EventByTypeSeverity(self, amount, type, severity):
        try:
            result = self.session.execute(
                "Select types.type_name as type, severities.severity_name as severity, cities.id as city, airport_code, "
                "start_time, end_time from weather_events "
                "inner join types on weather_events.type = (SELECT id from types WHERE type_name='{0}' LIMIT 1) "
                "inner join severities on weather_events.severity = (SELECT id from severities WHERE severity_name='{1}')"
                "inner join cities on weather_events.city = cities.id "
                "where type = (SELECT id from types WHERE type_name='{0}' LIMIT 1) "
                "and severity=(SELECT id from severities WHERE severity_name='{1}') limit {2}".format(type, severity, amount))
            weather_ev = result.fetchall()
            print(weather_ev[0])
            print("  Type |  Severity  |  City  |  Airport  |  Start time  |  End time")
            for i in range(0, len(weather_ev)):
                j = [weather_ev[i][0], weather_ev[i][1], int(weather_ev[i][2]), weather_ev[i][3],
                     weather_ev[i][4].strftime('%Y-%m-%d %H:%M:%S'), weather_ev[i][5].strftime('%Y-%m-%d %H:%M:%S')]
                print(j)
        except exc.SQLAlchemyError:
            print("Troubles in finding. Please check your input.")
            return False
        return True

    def EventByAirportType(self,amount, airport_code, type):
        try:
            result = self.session.execute(
                "Select severities.severity_name as severity, cities.city_name as city, airport_code, "
                "start_time, end_time,weather_events.id from weather_events "
                "inner join types on weather_events.type = (SELECT id from types WHERE type_name='{0}' LIMIT 1) "
                "inner join severities on weather_events.severity = severities.id "
                "inner join cities on weather_events.city = cities.id "
                "where type = (SELECT id from types WHERE type_name='{0}' LIMIT 1) and airport_code = '{1}' limit {2}".format(type, airport_code, amount))
            weather_ev = result.fetchall()
            print(weather_ev[0])
            print("  ID   |  Severity  |  City  |  Start time  |  End time")
            for i in range(0, len(weather_ev)):
                j = [int(weather_ev[i][5]), weather_ev[i][0], weather_ev[i][1],
                     weather_ev[i][3].strftime('%Y-%m-%d %H:%M:%S'),
                     weather_ev[i][4].strftime('%Y-%m-%d %H:%M:%S')]
                print(j)
        except exc.SQLAlchemyError:
            print("Troubles in finding. Please check your input.")
            return False
        return True

    def EventByStateSeverity(self, amount, state, severity):
        try:
            result = self.session.execute(
                "SELECT * FROM (SELECT id as county_id, state from counties WHERE counties.state=(SELECT id from states WHERE state_name='{0}')) "
                "as the_county INNER JOIN (SELECT id as city_id, county from cities) "
                "as the_city ON the_county.county_id = the_city.county "
                "INNER JOIN (SELECT id,type,severity,city,airport_code,start_time,end_time from weather_events "
                "WHERE severity =(SELECT id from severities WHERE severity_name='{1}')) "
                "as w_event ON the_city.city_id = w_event.city limit {2}".format(state, severity, amount))
            weather_ev = result.fetchall()
            print(weather_ev[0])
            print("  ID  |  State  |  Type  |  Airport  |  Start time  |  End time")
            for i in range(0, len(weather_ev)):
                j = [int(weather_ev[i][4]), int(weather_ev[i][1]), int(weather_ev[i][5]), weather_ev[i][8],
                     weather_ev[i][9].strftime('%Y-%m-%d %H:%M:%S'), weather_ev[i][10].strftime('%Y-%m-%d %H:%M:%S')]
                print(j)
        except exc.SQLAlchemyError:
            print("Troubles in finding. Please check your input.")
            return False
        return True

    def AnalyzeTypesByAirports(self):
        df = pd.read_sql_table('weather_events', self.engine,columns=['airport_code','type','id'])
        df2 = pd.read_sql_table('types', self.engine)
        j=len(df2.index)
        lst=[]
        ids=[]
        i=1
        for x in range(1, j+1):
            lst.append(int(len(df[df['type'] == i])))
            ids.append(i)
            i+=1
        df3 = pd.DataFrame(list(zip(lst, ids)), columns=['airports','id'])
        df_3 = pd.merge(df2, df3, how='inner', on='id')
        pplt.bar(df_3["type_name"], lst)
        pplt.ylabel("Amount of airports")
        pplt.xlabel("Types")
        pplt.suptitle('All analysis:')
        pplt.show()

    def AnalyzeSeveritiesByAirports(self):
        df = pd.read_sql_table('weather_events', self.engine, columns=['airport_code', 'severity', 'id'])
        df2 = pd.read_sql_table('severities', self.engine)
        j = len(df2.index)
        lst = []
        ids = []
        i = 1
        for x in range(1, j + 1):
            lst.append(int(len(df[df['severity'] == i])))
            ids.append(i)
            i += 1
        df3 = pd.DataFrame(list(zip(lst, ids)), columns=['airports', 'id'])
        df_3 = pd.merge(df2, df3, how='inner', on='id')
        pplt.bar(df_3["severity_name"], lst)
        pplt.ylabel("Amount of airports")
        pplt.xlabel("Severities")
        pplt.suptitle('All analysis:')
        pplt.show()
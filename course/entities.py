import datetime
import psycopg2
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy import Column, Integer, String, Numeric, DateTime, Table, ForeignKey

Base = declarative_base()


# 1
class Cities(Base):
    __tablename__ = 'cities'
    id = Column(Integer, primary_key=True)
    city_name = Column(String)
    county = Column(Integer, ForeignKey('counties.id'))
    county_ = relationship("Counties", backref="Cities")
    zip_code = Column(Integer)
    time_zone = Column(String)

    def __init__(self, city_name, county, zip_code, time_zone):
        self.city_name = city_name
        self.county = county
        self.zip_code = zip_code
        self.time_zone = time_zone


# 2
class Counties(Base):
    __tablename__ = 'counties'
    id = Column(Integer, primary_key=True)
    county_name = Column(String)
    state = Column(Integer, ForeignKey('states.id'))
    state_ = relationship("States", backref="Counties")

    def __init__(self, county_name, state):
        self.county_name = county_name
        self.state = state


# 3
class States(Base):
    __tablename__ = 'states'
    id = Column(Integer, primary_key=True)
    state_name = Column(String)

    def __init__(self, state_name):
        self.CountryId = state_name


# 4
class Severities(Base):
    __tablename__ = 'severities'
    id = Column(Integer, primary_key=True)
    severity_name = Column(String)

    def __init__(self, severity_name):
        self.severity_name = severity_name


# 5
class Types(Base):
    __tablename__ = 'types'
    id = Column(Integer, primary_key=True)
    type_name = Column(String)

    def __init__(self, type_name):
        self.type_name = type_name


# 6
class WeatherEvents(Base):
    __tablename__ = 'weather_events'
    id = Column(String, primary_key=True)
    start_time = Column(DateTime(True))
    end_time = Column(DateTime(True))
    airport_code = Column(String)
    location_lat = Column(Numeric)
    location_lng = Column(Numeric)
    severity = Column(Integer, ForeignKey('severities.id'))
    severity_ = relationship("Severities", backref="WeatherEvents")
    type = Column(Integer, ForeignKey('types.id'))
    type_ = relationship("Types", backref="WeatherEvents")
    city = Column(Integer, ForeignKey('cities.id'))
    city_ = relationship("Cities", backref="WeatherEvents")

    def __init__(self, start_time, end_time, airport_code, location_lat, location_lng,
                 severity, type, city):
        self.start_time = start_time
        self.end_time = end_time
        self.airport_code = airport_code
        self.location_lat = location_lat
        self.location_lng = location_lng
        self.severity = severity
        self.type = type
        self.city = city

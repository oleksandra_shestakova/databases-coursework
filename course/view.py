from python_console_menu import *
from colr import *
from orm import *
from entities import *

db = DataBase()
db.connect()


class CRUDMenu(AbstractMenu):
    def __init__(self, entityName_):
        self.entityName = entityName_
        super().__init__("Choose an option:")

    def getBool(self, input_):
        if (input_.lower() == "true" or int(input_) == 1):
            return True
        else:
            return False

    def AddEntityInfo(self, entityName_):
        entity = -1
        if entityName_ == "Cities":
            entity = Cities(input("City name: "), int(input("City county(id): ")),
                            int(input("City ZIP code:")), input("City time zone: "))
        elif entityName_ == "Counties":
            entity = Counties(input("County name: "), int(input("County state(id): ")))
        elif entityName_ == "States":
            entity = States(input("State name: "))
        elif entityName_ == "Severities":
            entity = Severities(input("Severity name: "))
        elif entityName_ == "Types":
            entity = Types(input("Type name: "))
        elif entityName_ == "WeatherEvents":
            entity = WeatherEvents(datetime.strptime(input("Start time: "), '%Y-%m-%d %H:%M:%S'),
                                   datetime.strptime(input("End time: "), '%Y-%m-%d %H:%M:%S'),
                                   input("Airport code: "), float(input("Location latitude: ")),
                                   float(input("Location longitude: ")), int(input("Severity(id): ")),
                                   int(input("Type(id): ")), int(input("City(id): ")))
        if entity == -1:
            print("Wrong entity name.")
            return False
        return db.AddEntity(entity)

    def UDEntityInfo(self, action, entityName_, entityId):
        entity = db.GetEntityById(entityName_, entityId)
        if action == "update":
            if entity == False:
                return False
            if entityName_ == "Cities":
                entity.city_name = input("City name: ")
                entity.county = int(input("City county(id): "))
                entity.zip_code = int(input("City ZIP code:"))
                entity.time_zone = input("City time zone: ")
                return db.UpdateCity(entity)
            if entityName_ == "Counties":
                entity.county_name = input("County name: ")
                entity.state = int(input("County state(id): "))
                return db.UpdateCounty(entity)
            if entityName_ == "States":
                entity.state_name = input("State name: ")
                return db.UpdateState(entity)
            if entityName_ == "Severities":
                entity.HotelId = input("Severity name: ")
                return db.UpdateSeverity(entity)
            if entityName_ == "Types":
                entity.HotelId = input("Type name: ")
                return db.UpdateType(entity)
            if entityName_ == "WeatherEvents":
                entity.start_time = datetime.strptime(input("Start time: "), '%Y-%m-%d %H:%M:%S')
                entity.end_time = datetime.strptime(input("End time: "), '%Y-%m-%d %H:%M:%S')
                entity.airport_code = input("Airport code: ")
                entity.location_lat = float(input("Location latitude: "))
                entity.location_lng = float(input("Location longitude: "))
                entity.severity = int(input("Severity(id): "))
                entity.type = int(input("Type(id): "))
                entity.city = int(input("City(id): "))
                return db.UpdateWeatherEvent(entity)
            print("Problem updating entity: given entity name was not found.")
            return False
        elif action == "delete":
            db.DeleteEntity(entity)

    def GenerateEntityInfo(self, entityName_, amount):
        if entityName_ == "WeatherEvents":
            return db.GenerateWeatherEvent(amount)
        print("There are no need in generating this entity. Please choose another entity.")

    def initialise(self):
        self.add_menu_item(MenuItem(100, "Exit menu").set_as_exit_option())
        self.add_menu_item(MenuItem(101, "Get entity", lambda: db.GetEntityById(self.entityName, int(input("Id:")))))
        self.add_menu_item(MenuItem(102, "Add entity", lambda: self.AddEntityInfo(self.entityName)))
        self.add_menu_item(
            MenuItem(103, "Update entity", lambda: self.UDEntityInfo("update", self.entityName, input("Id:"))))
        self.add_menu_item(
            MenuItem(104, "Delete entity", lambda: self.UDEntityInfo("delete", self.entityName, input("Id:"))))
        self.add_menu_item(MenuItem(105, "Generate entity", lambda: self.GenerateEntityInfo(self.entityName, input(
            "Amount of entities to generate:"))))

class EntityMenu(AbstractMenu):
    def __init__(self):
        super().__init__("Choose the entity to work with:")

    def initialise(self):
        self.add_menu_item(MenuItem(100, "Exit menu").set_as_exit_option())
        self.add_menu_item(MenuItem(101, "Cities", menu=CRUDMenu("Cities")))
        self.add_menu_item(MenuItem(102, "Counties", menu=CRUDMenu("Counties")))
        self.add_menu_item(MenuItem(103, "States", menu=CRUDMenu("States")))
        self.add_menu_item(MenuItem(104, "Severities", menu=CRUDMenu("Severities")))
        self.add_menu_item(MenuItem(105, "Types", menu=CRUDMenu("Types")))
        self.add_menu_item(MenuItem(106, "WeatherEvents", menu=CRUDMenu("WeatherEvents")))


class MainMenu(AbstractMenu):
    def __init__(self):
        super().__init__(color("---MAIN MENU---", fore='00f8fc'))

    def getBool(self, input_):
        if (input_.lower() == "true" or int(input_) == 1):
            return True
        else:
            return False

    def RouteFindFunc(self, action):
        if action == "TypeSeverity":
            return db.EventByTypeSeverity(int(input("Amount of records to get: ")),
                                          input("Weather event type name: "),
                                          input("Weather event severity name: "))
        elif action == "AirportType":
            return db.EventByAirportType(int(input("Amount of records to get: ")),
                                         input("Weather event airport code: "),
                                         input("Weather event type name: "))
        if action == "StateSeverity":
            return db.EventByStateSeverity(int(input("Amount of records to get: ")),
                                           input("Weather event state(abbreviation): "),
                                           input("Weather event severity name: "))

    def initialise(self):
        self.add_menu_item(MenuItem(100, color("Exit program", fore='917a81')).set_as_exit_option())
        self.add_menu_item(MenuItem(101, color("CRUD+generate entity menu", fore='ebc634'), menu=EntityMenu()))
        self.add_menu_item(MenuItem(102, color("Get weather events by type and severity", fore='03cafc'),
                                    lambda: self.RouteFindFunc("TypeSeverity")))
        self.add_menu_item(MenuItem(103, color("Get weather events by airport code and type", fore='03cafc'),
                                    lambda: self.RouteFindFunc("AirportType")))
        self.add_menu_item(MenuItem(104, color("Get weather events by state and severity", fore='03cafc'),
                                    lambda: self.RouteFindFunc("StateSeverity")))
        self.add_menu_item(MenuItem(105, color("Analyze types of events by amount of airports", fore='03cafc'),
                                    lambda: db.AnalyzeTypesByAirports()))
        self.add_menu_item(MenuItem(106, color("Analyze severities of events by amount of airports", fore='03cafc'),
                                    lambda: db.AnalyzeSeveritiesByAirports()))
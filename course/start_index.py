import psycopg2
import matplotlib.pyplot as plt
connect = psycopg2.connect(user='postgres',password='DipperS13',host='localhost',port='5432',database='USWeather')
cursor = connect.cursor()
results_without=[]
cursor.execute('drop index start_time_index ')
connect.commit()
for j in range(30):
    cursor.execute("explain analyze select * from weather_events where start_time between "
                   "'2018-01-01'::timestamp and '2019-01-01'::timestamp")
    connect.commit()
    res = cursor.fetchall()
    for i in res:
        if(i[0].find('Execution')>-1):
            result=None
            for j in i[0].split():
                try:
                    result = float(j)
                    break
                except:
                    continue
            results_without.append(result)
connect.commit()
cursor.execute('CREATE INDEX start_time_index ON weather_events USING btree (start_time)')

results_with=[]
for j in range(30):
    cursor.execute("explain analyze select * from weather_events where start_time between "
                   "'2018-01-01'::timestamp and '2019-01-01'::timestamp")
    connect.commit()
    res = cursor.fetchall()
    for i in res:
        if(i[0].find('Execution')>-1):
            result=None
            for j in i[0].split():
                try:
                    result = float(j)
                    break
                except:
                    continue
            results_with.append(result)
plt.plot(results_without)
plt.plot(results_with)
plt.show()
cursor.close()
connect.close()